#!/usr/bin/env python
# coding: utf-8

# In[16]:


# Preparation in this cell. Read import modules and read in the data
from matplotlib import pyplot as plt
import csv
import json
import numpy as np
import random
from collections import defaultdict
from collections import Counter

data=[]
with open("input.csv", "r+") as fp:
    csv_reader=csv.DictReader(fp)
    for row in csv_reader:
        row["video_id"]=row["video_id"]
        row["trending_date"]=row["trending_date"]
        row["title"]=row["title"]
        row["channel_title"]=row["channel_title"]
        row['category_id']=int(row['category_id'])
        row['publish_time']=row['publish_time']
        row['tags']=row['tags']
        row['views']=int(row['views'])
        row['likes']=int(row['likes'])
        row['dislikes']=int(row['dislikes'])
        row['comment_count']=int(row['comment_count'])
        row['thumbnail_link']=row['thumbnail_link']
        row['comments_disabled']=row['comments_disabled']
        row['ratings_disabled']=row['ratings_disabled']
        row['video_error_or_removed'] = row['video_error_or_removed']
        row['description']=row['description']
        
        data.append(row)

def jprint(data):
    print(json.dumps(data, indent=4))
    
jprint(data)


# In[17]:


# Q1 Figure out basic information including the number of videos, the number of unique channels, and total_views.
# Save result in "question1.csv"

number_of_videos = len(data) #first column of the table

unique_channels = defaultdict(list)

for d in data:
    unique_channels[d["channel_title"]].append(d)

number_of_uniquechannels = len(unique_channels) #second column of the table

total_views = sum([d["views"] for d in data])#third column of the table

#export csv file
q1_columns = ["number_of_videos", "number_of_unique_channels", "total_views"]# all columns list
q1_out_strs =[]
q1_out_strs.append(",".join(q1_columns))

my_dict = {
    "number_of_videos": number_of_videos,
    "number_of_unique_channels" : number_of_uniquechannels,
    "total_views": total_views
}

q1_line = []
for c in q1_columns:
    q1_line.append(str(my_dict[c]))
q1_line=",".join(q1_line)
q1_out_strs.append(q1_line)
# out_strs

with open("question1.csv","w+") as fp: #export out_str as csv file
    for line in q1_out_strs:
        fp.write(line+"\n")
q1_out_strs


# In[18]:


# Q2 How many videos are published in each year? Print out the year and number of videos in each year. Show the result in ascending order of the year.

# Save result in "question2.csv"

years_list = [] #list of years
for d in data:
    years_list.append(d['publish_time'][:4]) #iterate through the dictionary for publish time, but only take the first four characters for the year
count_dict = Counter(years_list) #count the number of years reoccuring

count_dict = sorted(count_dict.items()) #sorted the years in accending order
    
# count_dict
q2_columns = ['year','number_of_videos']
q2_out_strs = []
q2_out_strs.append(','. join(q2_columns))

#export csv file
for year in count_dict:
    q2_line = []
    list_count={
    'year' : 0,
    'number_of_videos' : 0
    }
    list_count['year'] = year[0]
    list_count['number_of_videos'] = year[1]
    for c in q2_columns:
        q2_line.append(str(list_count[c]))
    q2_line=','.join(q2_line)
#     print (q2_line)
    q2_out_strs.append(q2_line)
q2_out_strs
with open ('question2.csv','w+') as fp:
    for line in q2_out_strs:
        fp.write(line+'\n')


# In[19]:


# Q3 Channel analysis. Find the number of videos in each channel, total video count, total views, average views, max like number, min dislike number, positivity, and a list of tags used by videos of this channel. The positivity score of a channel is calculated by total likes/(total likes+ total dislikes). If no likes and dislikes the score is -1. The tags are unique tags. Sort the tags in alphabetic order and separate different tags by “|”.

# Save result in "question3.csv"

dict_list = [] #list of dictionaries containing all the columns
channel_list = [] #list for counter function

for d in data:
    channel_list.append(d['channel_title'])
no_videos = Counter(channel_list) #obtain the names of uniques channels and numbers of videos from these channels.

for channel in no_videos:
    views = [d['views'] for d in data if d['channel_title'] == channel] #collect all views belong to a channel
    likes_list = [d['likes'] for d in data if d['channel_title'] == channel] #collect all likes belong to a channel
    dislikes_list = [d['dislikes'] for d in data if d['channel_title'] == channel] #collect all dislikes belong to a channel
    tags = [d['tags'] for d in data if d['channel_title'] == channel] #collect all tags belong to a channel
    tags_list = []
    for t in tags:
        tags_list.extend(t.split('|')) #seprate tags into lists join all tags into a big list
        
    unique_tags = [c for c in Counter(tags_list)]
    
    tags_list='|'.join(sorted(unique_tags)) # join all tags with '|' sorted the tags alphabetically
    
    total_views = sum(views)
    max_likes = np.max(likes_list)
    min_dislikes = np.min(dislikes_list)
    if sum(likes_list) == 0 and sum(dislikes_list) == 0: #if sum of dislikes and likes are zero, the positivity score is -1
        positivity = -1
    else:
        positivity = sum(likes_list) / (sum(likes_list) + sum(dislikes_list))
    my_dict = {
        'channel_title': channel,
        'total_videos': no_videos[channel],
        'total_views': total_views,
        'average_views': total_views / len(views),
        'max_likes': max_likes,
        'min_dislikes': min_dislikes,
        'positivity': positivity,
        'tags': tags_list
    }
    dict_list.append(my_dict)
    
dict_list = sorted(dict_list, key = lambda x:x['channel_title'])

# export csv file

with open ('question3.csv', mode = 'w+', encoding = 'utf-8') as csv_fp:
    csv_writer = csv.writer(csv_fp,
                           delimiter=',',
                           quotechar='"',
                           quoting = csv.QUOTE_MINIMAL,
                           lineterminator = "\n")
    csv_writer.writerow(dict_list[0].keys())
    for d in dict_list:
        csv_writer.writerow(d.values())


# In[20]:


# Q4

# Save result in "question4.csv"

#For all videos with at least 1 like and more than 100,000 views, calculate the quality score and quality category. 
#The quality score of a video is calculated by likes/(likes+ dislikes). 
#If no likes and dislikes the score is -1. The quality category is:

score = 0
results_list=[]

def quality_categorizer(vid):
        if vid>=90:
            return "A"
        elif 80<=vid<90:
            return "B"
        elif 70<=vid<80:
            return "C"
        elif 60<=vid<70:
            return "D"
        else: 
            return "E"

for d in data:
    if d["likes"] >= 1 and d["views"]>100000: #For all videos with at least 1 like and more than 100,000 views
        title = [d["title"]]
        likes =  [d["likes"]]
        dislikes = [d["dislikes"]]
        denominator = likes[0] + dislikes[0]
    
        if denominator == 0:
            score = -1
        else:
            score = likes[0]/denominator
            score_perfect = score*100

    data_dict = {
        "title": title[0],
        "quality_score" : score,
        "quality_category":quality_categorizer(score_perfect)
    }
    if data_dict not in results_list: #categorize the score into rating
        results_list.append(data_dict)
        
results_list = sorted(results_list, key = lambda x:x["quality_score"], reverse = True)
jprint (results_list)
with open ('question4.csv', mode = 'w+', encoding = 'utf-8') as csv_fp:
    csv_writer = csv.writer(csv_fp,
                           delimiter=',',
                           quotechar='"',
                           quoting = csv.QUOTE_MINIMAL,
                           lineterminator = "\n")
    csv_writer.writerow(results_list[0].keys())
    for d in results_list:
        csv_writer.writerow(d.values())


# In[21]:


# Q5 Find all videos which use at least one popular tag. Popular tags are tags that appear in at least 10 different videos. Print the result in ascending order of the video_id (alphabetic)

# The output file should have the following header:

# video_id

# Save result in "question5.csv"
popular_tags = []
popular_videos = []

all_tags = []
videos_tags = [d['tags'] for d in data] #collect all tags from all videos
for t in videos_tags:
    all_tags.extend(t.split('|')) # combine these tags in to a big list
count_tags = Counter(all_tags) #count the number of tags

for t in count_tags:
    if count_tags[t] >= 10: #select popular tags which appear in more than then videos
        popular_tags.append(t)
        popular_videos.extend([d['video_id'] for d in data if t in d['tags']]) # combine a big list of videos contain popular tags  
popular_videos = Counter(popular_videos) #find list of all popular videos
popular_videos = sorted(popular_videos.items()) #Sort the list of videos

popular_videos

# export csv file
q5_columns = ['video_id']
q5_out_strs = []
q5_out_strs.append(','. join(q5_columns))


for videos in popular_videos:
    q5_line = []
    q5_line.append(str(videos[0]))
    q5_line=','.join(q5_line)
    q5_out_strs.append(q5_line)
q5_out_strs
with open ('question5.csv','w+') as fp:
    for line in q5_out_strs:
        fp.write(line+'\n')


# In[22]:


# Q6
# Show all categories with at least 10 videos. For each category, show the category name, number of videos, videoId with the highest views (if same, print the one that appears first in the CSV), average comment count, number of videos disabled comments, and a list of unique channels that published videos in the category (in ascending order alphabetically. separated by '|'). Save the results in the descending order of video count.
# Convert the category id to the actual category name. Since different countries have different category id encoding, your code should allow dynamically convert the category id to the category name. That means your code must directly read the categories from the category_id.txt file. Do not hard coding the categories in your code.
# Save result in "question6.csv"

lines = []
with open ('category_id.txt', 'r') as fp:
    line = fp.readline()
#    print (line.strip())
    while line:
#        print (line.strip())
        lines.append(line.strip())
        line = fp.readline()
lines 
category_dict = defaultdict(list) # dict that maps id to category
for l in lines:
    temp = l.split(' - ')
    category_dict[temp[0]].append(str(temp[1]))
    
# jprint(category_dict)

dict_list = []
categories_list = [d['category_id'] for d in data]
categories_count = Counter(categories_list)
popular_categories = [d for d in categories_count if categories_count[d] >= 10]
popular_categories 


for c in popular_categories:
    
    channels = Counter([d['channel_title'] for d in data if d['category_id'] == c])
    unique_channels = '|'.join(sorted(channels))
    
    most_view = sorted([d for d in data if d['category_id']==c], key = lambda x: x['views'], reverse = True )
    
    
    data_dict = {
    'category_name': category_dict[str(c)][0], #take string from the list
    'video_count': len([d for d in data if d['category_id'] == c]),
    'most_popular_video': most_view[0]['video_id'],
    'average_comment_count': np.mean([d['comment_count'] for d in data if d['category_id']== c]),
    'disable_comment_count': len([d for d in data if d['comments_disabled'] == 'True' and d['category_id']==c]),
    'channel': unique_channels
    }
    dict_list.append(data_dict)
    
dict_list = sorted(dict_list, key = lambda x:x['video_count'], reverse = True)
jprint(dict_list)
with open ('question6.csv', mode = 'w+', encoding = 'utf-8') as csv_fp:
    csv_writer = csv.writer(csv_fp,
                           delimiter=',',
                           quotechar='"',
                           quoting = csv.QUOTE_MINIMAL,
                           lineterminator = "\n")
    csv_writer.writerow(dict_list[0].keys())
    for d in dict_list:
        csv_writer.writerow(d.values())

